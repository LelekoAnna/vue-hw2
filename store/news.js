export const state = () => ({
    news: [],
    currentNews: {},
    totalCount: null
})

export const mutations = {
    setNews(state, news) {
        state.news = news
        state.totalCount = news.length
    },
    setCurrentNews(state, id) {
        state.currentNews = state.news.find(n => n.id == id)
    }
}

export const actions = {
    downloadNews({commit}) {
        const news = [
            { 
                    id: 1,
                    imgUrl: 'https://images.pexels.com/photos/1183992/pexels-photo-1183992.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
                    alt:'some text', 
                    title: 'My journey with Vue', 
                    content: ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi reprehenderit impedit, ullam est cum ad placeat vero eligendi similique quidem nostrum, odit repellendus ipsam cumque! Accusantium distinctio aperiam cupiditate excepturi?'
            },
            { 
                    id: 2,
                    imgUrl: 'https://images.pexels.com/photos/886521/pexels-photo-886521.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
                    alt:'some text', 
                    title: 'Blogging with Vue', 
                    content: ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi reprehenderit impedit, ullam est cum ad placeat vero eligendi similique quidem nostrum, odit repellendus ipsam cumque! Accusantium distinctio aperiam cupiditate excepturi?'
            },
            { 
                    id: 3,
                    imgUrl: 'https://images.pexels.com/photos/1209843/pexels-photo-1209843.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
                    alt:'some text', 
                    title: 'Why Vue is so fun', 
                    content: ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi reprehenderit impedit, ullam est cum ad placeat vero eligendi similique quidem nostrum, odit repellendus ipsam cumque! Accusantium distinctio aperiam cupiditate excepturi?'
            },
            { 
                    id: 4,
                    imgUrl: 'https://images.pexels.com/photos/542556/pexels-photo-542556.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
                    alt:'some text', 
                    title: 'My journey with Vue', 
                    content: ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi reprehenderit impedit, ullam est cum ad placeat vero eligendi similique quidem nostrum, odit repellendus ipsam cumque! Accusantium distinctio aperiam cupiditate excepturi?'
            },
            { 
                    id: 5,
                    imgUrl: 'https://images.pexels.com/photos/1568607/pexels-photo-1568607.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
                    alt:'some text', 
                    title: 'Blogging with Vue', 
                    content: ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi reprehenderit impedit, ullam est cum ad placeat vero eligendi similique quidem nostrum, odit repellendus ipsam cumque! Accusantium distinctio aperiam cupiditate excepturi?'
            },
            { 
                    id: 6,
                    imgUrl: 'https://images.pexels.com/photos/1194420/pexels-photo-1194420.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1',
                    alt:'some text', 
                    title: 'Why Vue is so fun', 
                    content: ' Lorem ipsum dolor, sit amet consectetur adipisicing elit. Commodi reprehenderit impedit, ullam est cum ad placeat vero eligendi similique quidem nostrum, odit repellendus ipsam cumque! Accusantium distinctio aperiam cupiditate excepturi?'
            }
        ]
        commit('setNews', news)
    },
    findNewsById({commit}, id) {
        commit('setCurrentNews', id)
    }
}

export const getters = {
    getNews: state => state.news,
    getCurrentNews: state => state.currentNews,
    getTotalCount: state => state.totalCount
}